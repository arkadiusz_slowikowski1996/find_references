﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

 #if UNITY_EDITOR

public class FindReferences : EditorWindow
{
	/// <summary>
	/// Component looked for. Might be UnityEngine.Object as well.
	/// </summary>
    private UnityEngine.Object wantedObject = new UnityEngine.Object();
	/// <summary>
	/// If true, the algorithm will find all references to any of the components the object is consisted of.
	/// </summary>
	private bool searchByTheGameObject = false;
	/// <summary>
	/// If false, the algorithm will search also in components like Joints etc.
	/// </summary>
	private bool searchOnlyInScripts = false;
	/// <summary>
	/// If true, the algorithm will search for references also in the rest of the components of a object of 'wantedComponent'.
	/// </summary>
	private bool searchInself = false;
	/// <summary>
	/// List keeping all found references.
	/// </summary>
    private List<Reference_Info> referencesInfos = new List<Reference_Info>();
	/// <summary>
	/// Idk
	/// </summary>
    private BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
	/// <summary>
	/// Field exists only because EditorGUI's scroll needs it to a proper functionality.
	/// </summary>
	private Vector2 scrollPos = Vector2.zero;

	[MenuItem("Window/Find References")]
    public static void ShowWindow()
    {
        GetWindow(typeof(FindReferences));
    }

    void OnGUI()
    {
		///Resets a value of 'wantedObject' when clicked.
        if (GUILayout.Button("Reset")) wantedObject = new UnityEngine.Object();

        wantedObject = (UnityEngine.Object)EditorGUILayout.ObjectField("Object looked for: ", wantedObject, wantedObject.GetType(), true);
        searchByTheGameObject = EditorGUILayout.Toggle("Search using GameObject: ", searchByTheGameObject);
        searchOnlyInScripts = EditorGUILayout.Toggle("Search only in scripts: ", searchOnlyInScripts);
        searchInself = EditorGUILayout.Toggle("Search also inside self: ", searchInself);

		///Runs the algorithm when clicked. 
        if (GUILayout.Button("Find references")) FindReferencesToAnObject();

		///That's how to make vertical scroll in Unity Editor i guess. 1/4
		EditorGUILayout.BeginVertical();
		///That's how to make vertical scroll in Unity Editor i guess. 2/4
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

		///True if referecenes were found, show results.
        if (referencesInfos.Count > 0)
        {
            EditorGUILayout.LabelField(referencesInfos.Count + " RESULTS");
            EditorGUILayout.Space();

            foreach (Reference_Info ri in referencesInfos)
            {
                EditorGUILayout.Space();

                EditorGUILayout.ObjectField("Component: ", ri.component, ri.component.GetType(), false);
                EditorGUILayout.LabelField("Field name: ", ri.name);

                if (ri.index > -1) EditorGUILayout.LabelField("Index: ", ri.index.ToString());
            }
		}

		///That's how to make vertical scroll in Unity Editor i guess. 3/4
		EditorGUILayout.EndScrollView();
		///That's how to make vertical scroll in Unity Editor i guess. 4/4
        EditorGUILayout.EndVertical();
    }

	/// <summary>
	/// A method getting values from fields.
	/// </summary>
	public void FindReferencesToAnObject()
    {
		///First of all - reset the list.
        referencesInfos.Clear();
		///Get all objects in the scene.
        GameObject[] allGameObjects = FindObjectsOfType<GameObject>();

		///Go through all found gameObjects.
        foreach (GameObject gameObject in allGameObjects)
		{
			///Shouldn't happen, but better safe than sorry.
			if (gameObject == null) continue;

			///Prevents from searching inself, if not wanted.
			try { if (!searchInself && (gameObject == wantedObject || gameObject == (GameObject)wantedObject)) continue; } catch { }

			///An array of components in a current gameObject. Consisted only of scripts (or not), depending on 'searchOnlyInScripts'
            Component[] components = searchOnlyInScripts ? gameObject.GetComponents<MonoBehaviour>() : gameObject.GetComponents<Component>();

			///Go through all found components.
            foreach (Component component in components)
            {
				///Shouldn't happen, but better safe than sorry (again).
				if (component == null) continue;

				///Get type of the component needed to get all members of the component's type.
                Type compType = component.GetType();
                MemberInfo[] members = compType.GetMembers(bindFlags);

				///Go through all found members.
                foreach (MemberInfo member in members)
				{
					///Shouldn't happen, but better safe than sorry (agaaain).
					if (member == null) continue;

					///Idk who came up with this, but you can't get a value of field and property using the same - inhrited - method. Ugh.
					object value = null;
                    try { value = member.MemberType == MemberTypes.Field ? ((FieldInfo)member).GetValue(component) : ((PropertyInfo)member).GetValue(component, null); } catch { }

					///That can happen.
                    if (value == null) continue;

					///Checks if the value is IEnumerable.
                    IEnumerable collection = null;
                    bool isOriginallyACollection = typeof(IEnumerable).IsAssignableFrom(member.GetType());

                    if (isOriginallyACollection)
                    {
						///If is, then we just cast the value and assing it to the 'collection'.
                        collection = (IEnumerable)value;
                    }
                    else
                    {
						///If not, we create new collection and as the only element we put the value. That's pretty retarded, but the code looks way better.
                        try { collection = new UnityEngine.Object[] { (UnityEngine.Object)value }; } catch { }
                    }

					///Checks if in the collection is a reference to our wantedObject, and if is - adds it to the list.
                    CheckIfElementsAreWanted(gameObject, component, member, isOriginallyACollection, collection);
                }
            }
        }
    }

	/// <summary>
	/// A method checking whether a value of a field is equal our 'wantedObject'.
	/// </summary>
	/// <param name="unityObject">The parent gameObject for the component</param>
	/// <param name="component">The component with script in which is the memeber.</param>
	/// <param name="member">The member currently being checked.</param>
	/// <param name="isAssignable">Was member originally a collection?</param>
	/// <param name="collection">Collection.</param>
	private void CheckIfElementsAreWanted(GameObject unityObject, Component component, MemberInfo member, bool isAssignable, IEnumerable collection)
    {
		///Obviously, if a collection is empty, we are not going to check it.
        if (collection == null) return;

		///Needed to keep track of the position of a value in the collection in case a field is in fact IEnumerable, not just made by us.
        int i = 0;

		///Checking every element of the collection. Most of the times the collection will be consisted of only one element.
        foreach (UnityEngine.Object element in collection)
        {
            bool _1 = false;
            bool _2 = false;

			///When searching by the component.
			try { _1 = !searchByTheGameObject && element == wantedObject; } catch { }
			///When searching by the gameObject.
            try { _2 = searchByTheGameObject && ((Component)element).gameObject == ((Component)wantedObject).gameObject; } catch { }

            if (_1 || _2)
            {
				///Creating new object and adding it to the list.
                Reference_Info ri = new Reference_Info(unityObject, component, member, isAssignable ? i : -1);
                referencesInfos.Add(ri);
            }

			///Pre-incrementation
            ++i;
        }
    }
}

public class Reference_Info
{
    public string name;
    public GameObject unityObject;
    public Component component;
    public int index;

    public Reference_Info(GameObject go, Component c, MemberInfo mi, int i)
    {
        name = mi.Name;
        unityObject = go;
        component = c;
        index = i;
    }
}
#endif